package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import org.w3c.dom.Text;

public class AutorizationActivity2 extends AppCompatActivity {
    EditText login,password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_autorization2);
        login = findViewById(R.id.loginEditText);
        password = findViewById(R.id.password);
    }
    public void enterClik(View view) {
        Intent Intent = new Intent(AutorizationActivity2.this,menuActivity.class);
        startActivity(Intent);
    }

    public void regClick(View view) {
        String mylogin = login.getText().toString();
        String mypassword = password.getText().toString();
        if (!mylogin.equals("") && !mypassword.equals("")) {
            if (mylogin.equals("admin") && mypassword.equals("111111")) {
                Intent Intent = new Intent(AutorizationActivity2.this, registrationActivity.class);
                startActivity(Intent);
            }
            else {
                Toast.makeText(this,"логин и пароль не верен",Toast.LENGTH_LONG).show();
            }
        }
        else {
            Toast.makeText(this,"заполни поля",Toast.LENGTH_LONG).show();
        }
    }


